package edu.ntust.downfile.activity;

import edu.ntust.downfile.R;
import edu.ntust.downfile.content.UrlContent;
import edu.ntust.downfile.task.DownloadApkTask;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
/**
 * 
 * @author Ssu-Wei Tang
 *
 */
public class MainActivity extends Activity {
	
	Button btnDownload;
	public static ProgressDialog mProgressDialog;
	public static EditText editTextApkPackageName;
	protected static final int PROGRESS = 0x10000;
	private static Context mContext;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mContext=this;
		findView();
	}

	private void findView() {
		btnDownload=(Button)findViewById(R.id.button_download);
		editTextApkPackageName=(EditText)findViewById(R.id.editTextApkPackageName);
		btnDownload.setOnClickListener(new OnClickListener(){
	            public void onClick(View v) {
	            	if(editTextApkPackageName.getText().toString().equals(""))
	            	{
	            		getOkDialog(MainActivity.this,R.string.dialogWarm,R.string.toast_enterPackagename,R.string.dialogOK);
//	            		Toast.makeText(MainActivity.this, R.string.toast_enterPackagename, Toast.LENGTH_LONG).show();
	            	}
	            	else
	            	{
	            		startDownload();
	            	}
	            }
	        });
		
	}
	
	private void startDownload() {
        new DownloadApkTask().execute(UrlContent.APK_CRAWL);
	}
	

	 public  static ProgressDialog showProgressDialog() {
		 mProgressDialog = new ProgressDialog(mContext);
		 mProgressDialog.setMessage("Downloading file..");
		 mProgressDialog.setCancelable(false);
//		 mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
//		 mProgressDialog.setProgressStyle(ProgressBar);
		 mProgressDialog.show();
			return mProgressDialog;
		}
	 
		public void getOkDialog(Context context,int title, int message, int dialogok) {
			AlertDialog.Builder builder = new AlertDialog.Builder(context);
			builder.setTitle(title);
			builder.setMessage(message);
			builder.setCancelable(false);
			builder.setPositiveButton(dialogok,	new android.content.DialogInterface.OnClickListener() {
						@Override
						public void onClick(android.content.DialogInterface dialog,int whichButton) {
							setResult(RESULT_OK);			
						}
					});
			builder.create();
			builder.show();
		}

}
