package edu.ntust.downfile.task;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;

import edu.ntust.downfile.activity.MainActivity;
import android.os.AsyncTask;
import android.util.Log;

public class DownloadApkTask extends AsyncTask<String, String, String> {

	/**
	 * 
	 * @author Ssu-Wei Tang
	 *
	 */
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		MainActivity.showProgressDialog();
	}

	@Override
	protected String doInBackground(String... aurl) {

		try {

			HttpClient client = new DefaultHttpClient();
			HttpPost httpRequest = new HttpPost(aurl[0]);
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("packageName",MainActivity.editTextApkPackageName.getText().toString()));
			httpRequest.setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
			HttpResponse response = new DefaultHttpClient()	.execute(httpRequest);
			InputStream in = response.getEntity().getContent();
//			int lenghtOfFile = in.available();
			FileOutputStream out = new FileOutputStream(new File("/sdcard/Download/"+ MainActivity.editTextApkPackageName.getText().toString()+ ".apk"));
			byte[] b = new byte[1024];
			int count = 0;
//			int total = 0;
//			System.out.println("lenghtOfFile  " + lenghtOfFile);
			while ((count = in.read(b)) != -1) {
				out.write(b, 0, count);
			}
			out.flush();
			in.close();
			out.close();

		} catch (Exception e) {
		}
		return null;

	}

	protected void onProgressUpdate(String... progress) {
		Log.d("ANDRO_ASYNC", progress[0]);
		MainActivity.mProgressDialog.setProgress(Integer.parseInt(progress[0]));
	}

	@Override
	protected void onPostExecute(String unused) {
		MainActivity.mProgressDialog.dismiss();
	}
}
